package com.epam.test.automation.java.practice4;

public class Task2 {

    public static int[] Transform(int[] array, SortOrder order) {
        if ((array == null) || (array.length == 0)) {
            throw new IllegalArgumentException();
        }

        if (Task1.isSorted(array, order))
        {
            for (int i = 0; i<array.length; i++)
            {
                array[i] = array[i] + i;
            }
        }

        return array;
    }


}
