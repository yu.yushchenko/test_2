package com.epam.test.automation.java.practice4;

public class Task3 {

    private Task3(){

    }

    public static int multiArithmeticElements(int a1, int t, int n) {

        if ((a1 == 0) || (t == 0) || (n == 0))
        {
            throw new IllegalArgumentException();
        }

        int result = a1;
        for (int i = 1; i<n; i++)
        {
            result *= a1+t*1;
        }

        return result;
    }
}
