package com.epam.test.automation.java.practice4;

public class Task1 {

    public static boolean isSorted(int[] array, SortOrder order) {
        if (( array == null) || (array.length == 0)) {
            throw new IllegalArgumentException();
        }

        for (int i = 0; i<array.length - 1; i++)
        {
            switch (order)
            {
                case ASC:
                    if (array[i] > array[i+1])
                    {
                        return false;

                    }
                    break;

                case DESC:
                    if (array[i] < array[i+1])
                    {
                        return false;
                    }
                    break;
            }


        }
        return true;
    }
}
