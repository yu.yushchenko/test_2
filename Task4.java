package com.epam.test.automation.java.practice4;

public class Task4 {

    private Task4()
    {

    }

    public static double sumGeometricElements(int a1, double t, int alim) {

        if ((a1 == 0) && (t == 0) && (alim == 0))
        {
            throw new IllegalArgumentException();
        }

        double result = 0;
        double nextEl = a1;
        if ((a1 > 0) && (t > 0) && (alim == 0)) {

            return Math.round(a1 / (1 - t));
        }

        while (nextEl>=a1)
        {
            result += nextEl;
            nextEl *=t;
        }

        return result;
    }
}
